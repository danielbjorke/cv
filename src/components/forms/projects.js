import React from "react";
import Superchat from "./projects/superchat";
import Portfolio from "./projects/portfolio";
import LoginPortal from "./projects/loginportal";
import Chameleon from "./projects/chameleon";
import Municipality from "./projects/municipality";
import MeFit from "./projects/mefit";
import TwitterML from "./projects/twitterml";
import ZaraML from "./projects/zaraml";

const Projects = () => {

    return (
        <div className="projectView">
            <Superchat />
            <br></br>
            <Portfolio />
            <br></br>
            <MeFit />
            <br></br>
            <TwitterML />
            <br></br>
            <Chameleon />
            <br></br>
            <ZaraML />
            <br></br>
            <Municipality />
            <br></br>
            <LoginPortal />
            <br></br>
        </div >
    )
};

export default Projects;