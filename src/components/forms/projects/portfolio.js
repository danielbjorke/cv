import React from "react";

const Portfolio = () => {

    return (
        <div className="portfolio">
            <h4>Portfolio</h4>
            <h6>Technologies: React, Firebase</h6>
            <p>The portfolio is a personal webpage, that displays information about me,
            some of my background and projects that I have worked on.
                The goal of the page is to give a quicker overview for potential customers, on my skills and experiences.<br></br><br></br>

                Note: This site is still under development, and in particular mobile view is still not good enough. So I am working on fixing that!</p>

            <a href="https://danielbjorke.no" target="_blank" rel="noopener noreferrer">
                WebApp link</a>.
            <br></br>
            <a href="https://gitlab.com/danielbjorke/cv" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.
        </div>
    )
};

export default Portfolio;