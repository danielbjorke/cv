import React from "react";

const TwitterML = () => {

    return (
        <div className="twitterml">
            <h4>Analyzing tweets with machine learning</h4>
            <h6>Technologies: Python</h6>
            <p>During my last semester at University I took a class in Machine Learning. In this project I were given the task to create a supervised learning algorithm from scratch.
            The goal were to create an algorithm that could detect if a provided tweet were positive, negative or neutral. To help create the algorith I got 10 000 tweets provided.
            </p>

            <a href="https://gitlab.com/machine-larning/twitter-sentiment-analysis" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.

        </div>
    )
};

export default TwitterML;