import React from "react";

const Superchat = () => {

    return (
        <div className="superchat">
            <h4>Superchat</h4>
            <h6>Technologies: React, Firebase</h6>
            <p>Superchat is a simple real time chatroom.
            To use the application you can login with either facebook or google.
            Frontend is built with React and the authentication part and database is built using Firebase services.
            This was a small project that I played with during my christmas holiday to learn more about React and Firebase. </p>

            <a href="https://danielbjorke.gitlab.io/superchat/" target="_blank" rel="noopener noreferrer">
                WebApp link</a>.
            <br></br>
            <a href="https://gitlab.com/danielbjorke/superchat" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.
        </div>
    )
};

export default Superchat;