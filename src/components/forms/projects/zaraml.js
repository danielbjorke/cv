import React from "react";

const ZaraML = () => {

    return (
        <div className="zaraml">
            <h4>Classify clothing with machine learning</h4>
            <h6>Technologies: Python, PyTorch</h6>
            <p>In this project I were given the task of creating a neural network to classify images of clothes with the help of the library PyTorch.
                To help train the algorithm I were provided with images of clothes from Zara.</p>

            <a href="https://gitlab.com/machine-larning/image-classification" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.

        </div>
    )
};

export default ZaraML;