import React from "react";

const Chameleon = () => {

    return (
        <div className="chameleon">
            <h4>Chameleon Web App</h4>
            <h6>Technologies: React, Redux, SignalR, Asp.Net Core, SQLServer</h6>
            <p>This is a personal project that I currently are working on together with
            a colleague. The end goal is a fullstack application that will become a game called
            Chameleon. The game will firstly become a mobile friendly website, but eventually I would like to build a react native application for mobile users. The
            technologies being used are react on the fronted, and on the backend it is Asp.Net
            Core together with a SQL server. As the application will be in real time, and let several
            games being played simultaneously we are using SignalR.</p>

            <a href="https://gitlab.com/danielbjorke/chamillionairewebapp" target="_blank" rel="noopener noreferrer">
                GitLab frontend link</a>.
            <br></br>
            <a href="https://gitlab.com/net4/chameleonquestionsapi" target="_blank" rel="noopener noreferrer">
                GitLab backend link</a>.
        </div>
    )
};

export default Chameleon;