import React from "react";

const LoginPortal = () => {

    return (
        <div className="loginportal">
            <h4>Login Portal</h4>
            <h6>Technologies: React, Redux</h6>
            <p>I wanted to learn more about React and Redux so I decided to build a login portal that I can reuse for future projects.
                The portal uses an external database for storing username and password.</p>

            <a href="https://gitlab.com/learning-js1/reactloginportal" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.
        </div>
    )
};

export default LoginPortal;