import React from "react";

const MeFit = () => {

    return (
        <div className="mefit">
            <h4>Experis Academy Case project - MeFit Web App</h4>
            <h6>Technologies: Angular, KeyCloak, Azure, Asp.Net Core, SQLServer</h6>
            <p>Experis Academy are completed with a three week project. The consultants were part of a three man development team, who were given the task of creating and deploying
            a full stack application. The task was to create a fitness portal. Backend needed to be
            created in Asp.Net and hosted on Azure with frontend being built in Angular.
            Authentication and Authorization had to be implemented with Keycloak.
            Frontend <br></br>
            At the beginning of the project I worked mostly on the backend creating the API, and deploying this to Azure. Eventually I also contributed on the frontend part of the project.</p>

            <a href="https://gitlab.com/Hauktek/mefit-frontend" target="_blank" rel="noopener noreferrer">
                GitLab frontend link</a>.
            <br></br>
            <a href="https://gitlab.com/Hauktek/mefit-api" target="_blank" rel="noopener noreferrer">
                GitLab backend link</a>.
        </div>
    )
};

export default MeFit;