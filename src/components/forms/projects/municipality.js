import React from "react";

const Municipality = () => {


    return (
        <div className="municipality">
            <h4>Municipality Information</h4>
            <h6>Technologies: Javascript, HTML, CSS</h6>
            <p>This group project were during my last semester at University. The project was about using an API with a JSON document
            with data concerning all Norwegian Municipals education level and unemployment
            statistics. Together with this data, the group were tasked with creating a dynamic web
            page in JavaScript to display the statistics graphically.</p>

            <a href="https://github.com/danielbjorke/semesteroppgave" target="_blank" rel="noopener noreferrer">
                GitLab link</a>.
        </div>
    )
};

export default Municipality;