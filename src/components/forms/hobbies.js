import React from "react";

const Hobbies = () => {

    return (
        <div className="hobbiesview">
            <div className="football">
                <h5>Football</h5>
                <p>My biggest passion is defiantly watching football, and following my favorite teams Liverpool FC and SK Brann!
                    I have also been playing football on and off most of my life, and when I were living in Bergen I played for Nordnes IL in 7th division. </p>
            </div>
            <div className="videogames">
                <h5>Video games</h5>
                <p>Video games has always been a big passion of mine, and spending much time gaming also spiked my interest for technology and programming. </p>
            </div>
            <div className="tech">
                <h5>Technology</h5>
                <p>All things tech fascinates me, from building computers to smart home gadgets and headphones. While working for Kjell & Company I got to learn and sell all kinds of cool things!</p>
            </div>
            <div class="exercise">
                <h5>Exercise</h5>
                <p>I try to stay in shape, and are currently training for a half marathon. Having a sedentary job, also pushes me to try stay active and healthy!</p>
            </div>
        </div>

    )
};

export default Hobbies;