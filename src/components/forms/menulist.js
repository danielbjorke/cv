import React from "react";

const Menulist = () => {

    return (
        <ul>
            <li>
                <button>Home</button>
            </li>
            <li>
                <button>About</button>
            </li>
            <li>
                <button>Projects</button>
            </li>
            <li>
                <button>Contact</button>
            </li>
        </ul>

    )
};

export default Menulist;