import React from "react";

const Work = () => {

    return (
        <div className="workview">
            <div className="experis">
                <h4>Experis Norway - IT Consultant</h4>
                <h5>August 2020 - Present</h5>
                <p>During Experis Academy's three month graduate program I got introduced to relevant technologies and frameworks to become a fullstack .Net developer.
                These include JavaScript (React, Angular, Vue), .Net frameworks and working with SQL.
                I also got the opportunity to work with cloud services such as Azure, Docker and KeyCloak.
                    To learn more about Experis Academy <a href="https://experisacademy.no/en" target="_blank" rel="noopener noreferrer">
                        click here</a>.</p>
            </div>
            <div className="kjell">
                <h4>Kjell & Company - Sales Representative</h4>
                <h5>April 2018 - September 2019</h5>
                <p>While working as a sales representative, I had the responsibility to help and follow up customers. Kjell & Company sells technical products, so it was important to stay up to date on new technologies and appreciate learning.
                    I also had long periods of being acting store manager during holidays with very good results.<br></br>
                    To learn more about Kjell & Company <a href="https://www.kjell.com/no/om-oss/om-kjell-company" target="_blank" rel="noopener noreferrer">
                        click here</a>.</p>
            </div>
        </div>
    )
};

export default Work;