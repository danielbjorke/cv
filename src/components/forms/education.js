import React from "react";

const Education = () => {

    return (
        <div className="educationView">
            <div className="uib">
                <h4>University of Bergen</h4>
                <h5>Bachelors degree in Information Science</h5>
                <p>During my bachelors degree I got a good foundation of theoretical knowledge and practical experience working with different technologies.
                I got to work with both backend, frontend and database languages. Subjects includes, but are not limited to; machine learning in python, web development in JavaScript, Node.JS, MongoDB and HTML/CSS. I also obtained good knowledge about developing algorithms and data structures.
                </p>

                <p>To learn more about my bachelors degree and full list of subjects, check out University of Bergen's official page <a href="https://www.uib.no/studier/BASV-INFO" target="_blank" rel="noopener noreferrer">
                    here</a>.
                </p>
            </div>
        </div>

    )
};

export default Education;