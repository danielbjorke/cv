import React from "react";
import { FaLinkedinIn, FaGitlab } from "react-icons/fa";

const Footer = () => (
    <div className="footer">
        <a className="footericon" href="https://www.linkedin.com/in/danielbj%C3%B8rke/" target="_blank" rel="noopener noreferrer">
            <FaLinkedinIn />
        </a>
        <a className="footericon" href="https://gitlab.com/danielbjorke" target="_blank" rel="noopener noreferrer">
            <FaGitlab />
        </a>
    </div>
);

export default Footer;