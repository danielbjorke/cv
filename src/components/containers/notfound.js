import React from "react";
import { useHistory } from 'react-router-dom';
import { motion } from "framer-motion";

const NotFound = () => {
    const history = useHistory();
    return (
        <div>
            <motion.div class="notfound"
                animate={{ scale: 1.4, x: 900, y: 200 }}>
                <h1>You went the wrong way!</h1>
                <button className="btn btn-outline-dark" onClick={() => history.push("/")}>Go back home</button>
            </motion.div>
            <svg class="nfsvg" width="800" height="400">
                <rect width="800" height="400" />
            </svg>
        </div>



    )
};

export default NotFound;