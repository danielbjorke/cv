import React, { useState } from "react";
import { db } from "../../utils/firebase";

const Contact = () => {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
    const [loading, setLoading] = useState(false);
    const [sendingError, setSendingError] = useState("");

    const submitClicked = (e) => {
        e.preventDefault();
        setLoading(true);
        setSendingError("");

        db.collection("contacts").add({
            name: name,
            email: email,
            message: message,
        })
            .then(() => {
                alert("Thank you for sending me a message!")
                setLoading(false);
            })
            .catch(error => {
                alert(error.message);
                setLoading(false);
                setSendingError(error.message || error);
            });
        setName("")
        setEmail("")
        setMessage("")
    };


    return (
        <form className="form-group" onSubmit={submitClicked}>

            <h1>Talk to me!</h1>

            <label>Name</label>
            <input
                class="form-control"
                placeholder="name"
                minlength="2"
                value={name}
                onChange={(e) => setName(e.target.value)}
            ></input>

            <label>Email</label>
            <input
                className="form-control"
                placeholder="email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            ></input>
            <small className="form-text text-muted">I wont share your email with anyone!</small>

            <label>Message</label>
            <textarea
                class="form-control"
                placeholder="message..."
                minlength="5"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
            ></textarea>

            {sendingError && <p>{sendingError}</p>}

            {!loading ?
                <button type="submit" className="btn btn-outline-dark">Send</button> :
                <button className="btn btn-outline-dark" type="button" disabled>
                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    <span className="visually-hidden">Loading...</span>
                </button>
            }




        </form>

    )
};

export default Contact;