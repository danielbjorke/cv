import React from "react";
import { motion } from "framer-motion";
import { useHistory } from 'react-router-dom';


const Home = () => {

    const history = useHistory();

    return (
        <div className="home">
            <motion.h1 drag>Hi,</motion.h1>
            <h2>I'm Daniel.</h2>
            <p>Please learn more about <motion.span
                whileHover={{ scale: 1.1 }}
                id="about" type="button"
                onClick={() => history.push('/about')}>me</motion.span> or get in <motion.span
                    whileHover={{ scale: 1.1 }}
                    id="contact" type="button" onClick={() => history.push('/contact')}>touch</motion.span>!</p>
        </div>

    )
};

export default Home;