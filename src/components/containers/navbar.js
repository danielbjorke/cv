import React from 'react';
import { useHistory } from 'react-router-dom';
import { motion } from "framer-motion";

const Navbar = () => {
    const history = useHistory();
    const homeclick = () => history.replace('/');
    const aboutclick = () => history.push("/about");
    const contactclick = () => history.push("/contact");

    return (
        <nav className="navbar justify-content-between">
            <div>
                <p className="navs" type="button" onClick={homeclick}>db</p>
            </div>

            <div className="navRight">
                <motion.p className="navs" type="button" onClick={aboutclick}
                    whileHover={{ scale: 1.1 }}
                >About Me</motion.p>
                <motion.p className="navs" type="button" onClick={contactclick}
                    whileHover={{ scale: 1.1 }}
                >Contact</motion.p>
            </div>
        </nav>
    )
};

export default Navbar;