import React, { useState } from "react";
import Work from "../forms/work";
import Education from "../forms/education";
import Projects from "../forms/projects";
import Hobbies from "../forms/hobbies";
import { useHistory } from 'react-router-dom';
import { motion } from "framer-motion";

const About = () => {

    const history = useHistory();

    const [showWork, setShowWork] = useState(false);
    const [showEducation, setShowEducation] = useState(false);
    const [showProjects, setShowProjects] = useState(false);
    const [showHobbies, setShowHobbies] = useState(false);

    const onWorkClick = () => {
        setShowWork(!showWork);
        setShowEducation(false);
        setShowProjects(false);
        setShowHobbies(false);
    }

    const onEdClick = () => {
        setShowWork(false);
        setShowEducation(!showEducation);
        setShowProjects(false);
        setShowHobbies(false);
    }

    const onProjectClick = () => {
        setShowWork(false);
        setShowEducation(false);
        setShowProjects(!showProjects);
        setShowHobbies(false);
    }

    const onHobbiesClick = () => {
        setShowWork(false);
        setShowEducation(false);
        setShowProjects(false);
        setShowHobbies(!showHobbies);
    }


    return (
        <div className="aboutme">
            <div className="aboutmetitle">
                <motion.h1
                    initial={{ x: -1000 }}
                    animate={{ x: 0 }}
                >About me</motion.h1>
                <img id="cartoon" className="img-fluid" src="https://i.ibb.co/bKt0BM3/cartoon.jpg" alt="cartoon daniel" />
            </div>

            <motion.p
                initial={{ x: 1000 }}
                animate={{ x: 0 }}
            >My name is Daniel and I'm a 28 year old programmer currently working for Experis as a consultant.
            On this page I have summarized some of my accomplishments so far. For more details don't hesitate to <span type="button" onClick={() => history.push('/contact')}>contact</span> me!</motion.p>



            <div>
                <motion.button className="btn btn-outline-dark" type="button" onClick={onWorkClick}
                    whileHover={{
                        scale: 1.1
                    }}
                >Work</motion.button>

                <motion.button
                    whileHover={{
                        scale: 1.1
                    }}
                    className="btn btn-outline-dark" type="button" onClick={onEdClick}>Education</motion.button>

                <motion.button
                    whileHover={{
                        scale: 1.1
                    }}
                    className="btn btn-outline-dark" type="button" onClick={onProjectClick}>Projects</motion.button>


                <motion.button
                    whileHover={{
                        scale: 1.1
                    }}
                    className="btn btn-outline-dark" type="button" onClick={onHobbiesClick}>Hobbies</motion.button>
            </div>

            {showWork && <Work />}
            {showEducation && <Education />}
            {showProjects && <Projects />}
            {showHobbies && <Hobbies />}

        </div>

    )
};

export default About;