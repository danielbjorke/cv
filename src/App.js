import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from "../src/components/containers/navbar";
import Home from "./components/containers/home";
import About from "../src/components/containers/about";
import Contact from "../src/components/containers/contact";
import Footer from "../src/components/containers/footer";
import NotFound from "../src/components/containers/notfound";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route component={NotFound} />

        </Switch>
        <Footer />
      </div>

    </Router>

  );
}

export default App;
